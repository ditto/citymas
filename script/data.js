var exports = module.exports = {};

exports.consts = {
	STAT_SUM: 200, // 스탯 분배치
    STAT_MAX: 100, // 스탯당 최댓값
    INITIAL_POINT: 100, // 초기 행동력
    MOVE_POINT: 10, // 이동시 소모 포인트
    START_MONTH: 11,
    NWEEKS: 30,
    MAX_SURVEYS: 5, // 여론조사 가능 횟수
};

exports.stats = [
    {name: '경제', id: 'economy'},
    {name: '교육', id: 'education'},
    {name: '복지', id: 'welfare'},
    {name: '환경', id: 'environ'},
    {name: '문화', id: 'culture'},
];

exports.supportrules = {
	normal: {economy: 2, education: 2, welfare: 2, environ: 2, culture: 2},
	recession: {economy: 5, education: 1, welfare: 3, environ: -1, culture: 1},
	environment: {economy: -3, education: 1, welfare: 2, environ: 5, culture: 1},
	welfare: {economy: 2, education: 3, welfare: 5, environ: 1, culture: 1},
	culture: {economy: 1, education: 1, welfare: 1, environ: 1, culture: 5},
	education: {economy: 1, education: 5, welfare: 3, environ: 1, culture: 1},
};

exports.statName = function(id) {
	var name;
	exports.stats.forEach(function(stat) {
		if (id == stat.id)
			name = stat.name;
    });
    return name;
}

exports.areas = [
    {name: '종로/중구/용산', pos: [240,185], actions: ['street','market','museum','welfarefac',]},
    {name: '도봉/강북/성북/노원', pos: [280,110], actions: ['street','university','market',]},
    {name: '동대문/중랑/성동/광진', pos: [310,180], actions: ['street','market','enterprise',]},
    {name: '강동/송파', pos: [390,255], actions: ['street','school','enterprise','welfarefac',]},
	{name: '서초/강남', pos: [322,310], actions: ['street','school','enterprise',]},
    {name: '동작/관악/금천', pos: [192,313], actions: ['street','school','university',]},
    {name: '강서/양천/영등포/구로', pos: [160,246], actions: ['street','factory','enterprise',]},
    {name: '은평/마포/서대문', pos: [190,180], actions: ['street','university','hospital','market',]},
];

/*
언변 - 유세 시 효과가 10% 증가
전직법관 - 법 스탯 +10
성공한사업가 - 경제 스탯 +10
사회운동가 - 복지 +10
환경운동가 - 환경 +10
예술가 - 문화 +10
근면 - 행동력 + 15
*/

exports.perks = [
    {name: '언변', id: 'fluent'},
    {name: '환경운동가', id: 'envactiv'},
    {name: '사회운동가', id: 'socactiv'},
    {name: '전직 사업가', id: 'oribusin'},
    {name: '전직 예술가', id: 'oriartis'},
    {name: '근면', id: 'diligent'},
];
