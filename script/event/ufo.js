var gameData = require('../data');
var up_or_down;

module.exports = {
	name: '서울시에 UFO 목격!',
		bg: 'ufo.jpg',
	willHappen: function(state) {
		return Math.random() < 0.02; // 3%
	//	return true;
    },
	scenario: function(state) {
		
		if (Math.floor(Math.random()*100) <= 50)
		{
			up_or_down = 0;
			return [
				'UFO목격 후에 집단 최면에 걸립니다. 사람들이 당신을 좋아해요!',
			];
		}
		else 
		{
			up_or_down = 1;
			return [
					'UFO 목격후에 사람들이 이상해진 기분이 들어요. 아이고 맙소사! ',
				];
		}
    },
    reactions: function(state) {
    	return [];
    },
    applyEffect: function(state, reaction) {
		if(0==up_or_down)
		{
			state.stat['environ'] += 1;
			state.stat['economy'] += 1;
			state.stat['welfare'] += 1;
			state.stat['culture'] += 1;
			state.stat['education'] += 1;
			return '모든 능력치가 1 올라갔습니다.';
		}
		
		else
		{
			state.stat['environ'] -= 1;
			state.stat['economy'] -= 1;
			state.stat['welfare'] -= 1;
			state.stat['culture'] -= 1;
			state.stat['education'] -= 1;

			return '모든 능력치가 1 내려갔습니다.';
		}
    }
};
