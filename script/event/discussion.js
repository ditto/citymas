﻿var gameData = require('../data');

module.exports = {
	name: '토론회',
	willHappen: function(state) {
		return Math.random() < 0.1; // 10%
	//	return true;
	},
	scenario: function(state) {
		return [
		    'TV 토론회에 출연하게 되었습니다.',
		    '어떤 부분에 대해 주력 하시겠습니까?',
		];
    },
    reactions: function(state) {
    	return [
    	    '경제를 강조한다.',
    	    '복지를 강조한다.'
    	];
    },
    applyEffect: function(state, reaction) {
    	var inc;
    	if (reaction == 0)
    		inc = 'economy';
    	else
    		inc = 'welfare';
    	state.stat[inc] += 2;
    	return gameData.statName(inc) + ' 능력치가 2 올랐습니다.';
    }
};
