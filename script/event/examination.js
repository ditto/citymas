var gameData = require('../data');
var up_or_down;

module.exports = {
	name: '수능 시험 실시',
	bg: 'examination.jpg',
	willHappen: function(state) {

	if ( 1 == state.week) 
		return true;
		
	else
		return false;
    },
	scenario: function(state) {
		    if (Math.floor(Math.random()*100) <= state.stat['education'])
		{
			up_or_down = 0;
			return [
				'수능주간에 맞춰 교육정책을 발표합니다.',
				'학부모 및 학생들이 지지합니다',
			];
		}
		else 
		{
			up_or_down = 1;
			return [
				'수능주간에 맞춰 교육정책을 발표합니다.',
				'그러나 관계자들은 현실성이 없다고 외면합니다.',
				];
		}
    },
    reactions: function(state) {
    	return [];
    },
    applyEffect: function(state, reaction) {
		if(0==up_or_down)
		{
			state.stat['education'] += 1;
			state.stat['welfare'] += 1;
			return '교육,복지 능력치가 1 올라갔습니다.';
		}
		
		else
		{
			state.stat['education'] -= 1;
			state.stat['welfare'] -= 1;
			return '교육,복지 능력치가 1 내려갔습니다.';
		}
    }
};
