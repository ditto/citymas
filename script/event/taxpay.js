﻿var gameData = require('../data');
var up_or_down;

module.exports = {
	name: '후보자 청문회가 열렸습니다.',
	bg: 'judgement.jpg',
	willHappen: function(state) {
		return Math.random() < 0.05; // 4%
	//	return true;
	},
	scenario: function(state) {
		return [
		    '시장 후보자 청문회가 열렸습니다.',
		    '이번 주제는 세금납부 입니다',
		];
    },
    reactions: function(state) {
    	return [
    	    '전 털어도 먼지하나 안나옵니다!.',
    	    '조용히 묻어가자...'
    	];
    },
    applyEffect: function(state, reaction) {
    	var inc;
		up_or_down = Math.floor(Math.random()*100);
    	if (reaction == 0)			
			if(up_or_down <= 60)
			    	{
					for (var i=0; i<state.areas.length; i++)
						state.areas[i].popularity += 3;
					return '깨끗한 시장후보의 이미지를 구축합니다.';
					}
			else
					{
					for (var i=0; i<state.areas.length; i++)
						state.areas[i].popularity -= 4;
					return '개소리 집어쳐!.'+'세금포탈 혐의가 수면으로 떠오릅니다';
					}
		else			
			if(up_or_down <= 60)
			    	{
					for (var i=0; i<state.areas.length; i++)
						state.areas[i].popularity += 1;
					return '가만히 있으면 절반은 갑니다. 인생의 진리죠.';
					}
			else
					{
					for (var i=0; i<state.areas.length; i++)
						state.areas[i].popularity -= 3;
					return '부끄러운줄 알아야지!';
					}
		}
};
