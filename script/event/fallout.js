var gameData = require('../data');
var up_or_down;

module.exports = {
	name: '원전 사고',
		bg:'uranuim.jpg',
	willHappen: function(state) {
		return Math.random() < 0.02; // 3%
	//	return true;
    },
	scenario: function(state) {
		
		if (Math.floor(Math.random()*100) <= state.stat['environ'])
		{
			up_or_down = 0;
			return [
				'원전에서 사고가 발생했습니다. 당신의 의견으로 원전사태를 재빨리 수습했습니다.',
			];
		}
		else 
		{
			up_or_down = 1;
			return [
					'원전에서 사고가 발생했습니다. 당신의 예상은 틀렸어요!. 아이고ㅠㅠ 이제 후쿠시마 꼴 나겠네!!',
				];
		}
    },
    reactions: function(state) {
    	return [];
    },
    applyEffect: function(state, reaction) {
		if(0==up_or_down)
		{
			state.stat['environ'] += 1;
			state.stat['economy'] += 1;
			return '환경 능력치가 1 올라갔습니다.경제 능력치가 1 올라갔습니다.';
		}
		
		else
		{
			state.stat['environ'] -= 2;
			return '환경 능력치가 2 내려갔습니다.';
		}
    }
};
