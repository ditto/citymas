var gameData = require('../data');
var up_or_down;

module.exports = {
	name: '우박이 떨어집니다',
		bg: 'hail.jpg',
	willHappen: function(state) {
		return Math.random() < 0.04; // 3%
	//	return true;
    },
	scenario: function(state) {
		
		if (Math.floor(Math.random()*100) <= state.stat['environ'])
		{
			up_or_down = 0;
			return [
				'갑작스런 우박으로 사람들이 당황합니다',
				'당신이 평소에 주장한 환경 정책이 이슈로 떠오릅니다',
			];
		}
		else 
		{
			up_or_down = 1;
			return [
					'갑작스런 우박으로 피해가 속출합니다',
					'환경 정책에 신경쓰지 않은 당신을 사람들이 믿지 못합니다',
				];
		}
    },
    reactions: function(state) {
    	return [];
    },
    applyEffect: function(state, reaction) {
		if(0==up_or_down)
		{
			state.stat['environ'] += 1;
			return '환경 능력치가 1 올라갔습니다.';
		}
		
		else
		{
			state.stat['environ'] -= 1;
			return '환경 능력치가 1 내려갔습니다.';
		}
    }
};
