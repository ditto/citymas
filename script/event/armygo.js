﻿var gameData = require('../data');
var up_or_down;

module.exports = {
	name: '후보자 청문회가 열렸습니다.',
	bg: 'judgement.jpg',
	willHappen: function(state) {
		return Math.random() < 0.05; // 4%
	//	return true;
	},
	scenario: function(state) {
		return [
		    '시장 후보자 청문회가 열렸습니다.',
		    '이번 주제는 후보자의 병역문제 입니다.',
		];
    },
    reactions: function(state) {
    	return [
    	    '전 국방의 의무를 충실히 수행했습니다.',
    	    '말을 흐린다.'
    	];
    },
    applyEffect: function(state, reaction) {
    	var inc;
		up_or_down = Math.floor(Math.random()*100);
    	if (reaction == 0)			
			if(up_or_down <= 50)
			    	{
					for (var i=0; i<state.areas.length; i++)
						state.areas[i].popularity += 5;
					return '당당한 군필자군요! 인기가 올라갑니다.';
					}
			else
					{
					for (var i=0; i<state.areas.length; i++)
						state.areas[i].popularity -= 5;
					return '왜 멀쩡한데 공익을 갔을까... 인기가 떨어집니다.';
					}
		else			
			if(up_or_down <= 50)
			    	{
					for (var i=0; i<state.areas.length; i++)
						state.areas[i].popularity += 3;
					return '...일단은 넘어갑시다.';
					}
			else
					{
					for (var i=0; i<state.areas.length; i++)
						state.areas[i].popularity -= 3;
					return '뭔가 걸리는게 있나봐요? 말을 해봐요!';
					}
		}
};
