var gameData = require('../data');
var up_or_down;

module.exports = {
	name: '지진 발생!',
	bg : 'earthquake.jpg',
	willHappen: function(state) {
		return Math.random() < 0.06; // 3%
	//	return true;
    },
	scenario: function(state) {
		
		if (Math.floor(Math.random()*100) <= state.stat['environ'])
		{
			up_or_down = 0;
			return [
				'지진이 발생했습니다.',
				state.character.name+'후보님의 재해 대책 관련 공약이 주목을 받게 됩니다',
			];
		}
		else 
		{
			up_or_down = 1;
			return [
					'지진이 발생했습니다.',
					'평소에 환경에 신경을 쓰지 않은것이 화를 불러옵니다.',
				];
		}
    },
    reactions: function(state) {
    	return [];
    },
    applyEffect: function(state, reaction) {
		if(0==up_or_down)
		{
			state.stat['environ'] += 2;
			return '환경 능력치가 2 올라갔습니다.';
		}
		
		else
		{
			state.stat['environ'] -= 2;
			return '환경 능력치가 2 내려갔습니다.';
		}
    }
};
