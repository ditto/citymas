﻿var gameData = require('../data');
var up_or_down;

module.exports = {
	name: 'DDOS공격 발생',
	bg: 'ddos.jpg',
	willHappen: function(state) {
		return Math.random() < 0.04; // 4%
	//	return true;
	},
	scenario: function(state) {
		return [
		    'DDOS공격이 발생하였습니다.',
		    '신문에서 인터뷰를 요청합니다.',
		];
    },
    reactions: function(state) {
    	return [
    	    '북한의 소행이다.',
    	    '내부의 소행이다.'
    	];
    },
    applyEffect: function(state, reaction) {
    	var inc;
		up_or_down = Math.floor(Math.random()*100);
    	if (reaction == 0)			
			if(up_or_down <= 50)
			    	{
					for (var i=0; i<state.areas.length; i++)
						state.areas[i].popularity += 5;
					return 'DDOS는 북한의 소행으로 밝혀졌습니다.'+'이 일로 인해 유명세를 탑니다!';
					}
			else
					{
					for (var i=0; i<state.areas.length; i++)
						state.areas[i].popularity -= 5;
					return 'DDOS는 사실 비서의 소행이었습니다.'+'비난을 피하지 못할것입니다'+'인지도가 하락합니다';
					}
		else			
			if(up_or_down <= 50)
			    	{
					for (var i=0; i<state.areas.length; i++)
						state.areas[i].popularity += 5;
					return 'DDOS는 비서의 소행으로 밝혀졌습니다.'+'이 일로 인해 유명세를 탑니다!';
					}
			else
					{
					for (var i=0; i<state.areas.length; i++)
						state.areas[i].popularity -= 5;
					return 'DDOS는 사실 북한의 소행이었습니다'+'비난을 피하지 못할것입니다'+'인지도가 하락합니다';
					}
		}
};
