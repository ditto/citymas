var gameData = require('../data');
var up_or_down;

module.exports = {
	name: '주식시장 변동',
	bg: 'stock.jpg',
	willHappen: function(state) {
		return Math.random() < 0.4; // 10%
	//	return true;
    },
	scenario: function(state) {
		
		if (Math.floor(Math.random()*100) <= state.stat['economy'])
		{
			up_or_down = 0;
			return [
				'주식 시장이 활기를 띱니다!',
			];
		}
		else 
		{
			up_or_down = 1;
			return [
					'제가 언제 경제 살리겠다고 말이나 했습니까...',
				];
		}
    },
    reactions: function(state) {
    	return [];
    },
    applyEffect: function(state, reaction) {
		if(0==up_or_down)
		{
			state.stat['economy'] += 3;
			return '경제 능력치가 3 올라갔습니다.';
		}
		
		else
		{
			state.stat['economy'] -= 3;
			return '경제 능력치가 3 내려갔습니다.';
		}
    }
};
