var up_or_down;
var cases;

module.exports = {
	name: '대학 방문',
	point: 17,
	bg: 'univ.jpg',
	scenario: function(state) {
		up_or_down = Math.floor(Math.random()*100);
	
	if(up_or_down <= 15){
	cases = 2;
	return [
		    '대학을 방문하자, 마침 축제 기간입니다',
			'많은 학생들을 만나고 많은 이야기를 듣고 갑니다.',
		];
	}
	else
	{
	cases = 1;
	return [
		    '여러분 안녕하세요! 기호 1번 ' + state.character.name + ' 후보입니다.',
		    '여러분은 이나라의 미래입니다',
			'주변에 학생 동무들을 많이 데려오시오',
			'...대학 배경이 전부 고대라고 화내지 마세요...ㅠㅠ',
		];}
    },
    applyEffect: function(state) {
		state.areas[state.region].popularity += 2;
		state.stat['education'] += 1+cases;
		state.stat['welfare'] +=cases;
		state.acts['education'] += 1;
		return '현재 지역의 인지도가 올랐습니다.',  
		'교육,복지 능력치가 올랐습니다.';
    }
};
