var gameData = require('../data');

module.exports = {
	name: '여론조사',
	point: 40,
	scenario: function(state) {
        state.lastSurveyResult = [];
        for (var i = 0; i < gameData.areas.length; i++) {
            // 오차를 +-5% 적용.
            var support = state.areas[i].support + (-5 + Math.random()*10);
            if (support < 0)
                support = state.areas[i].support * Math.random();
            //support /= 10;
            state.lastSurveyResult.push(support);
        }
        return [ '여론조사를 실시하였습니다.', '결과는 왼쪽 정보창 맨 아래에서 확인할 수 있습니다.' ];
    },
    applyEffect: function(state) {
        state.surveyCount++;
        state.lastSurveyWeek = state.week;
    	return '앞으로 여론조사를 ' + (gameData.consts.MAX_SURVEYS - state.surveyCount) + '번 더 할 수 있습니다.';
    }
};
