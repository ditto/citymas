module.exports = {
	name: '박물관 방문',
	point: 15,
	bg : 'museum.jpg',
	scenario: function(state) {
		return [
		    '여러분 안녕하세요! 기호 1번 ' + state.character.name + ' 후보입니다.',
		    '박물관 같은 문화 시설 확충에 힘쓰겠습니다.',
		];
    },
    applyEffect: function(state) {
		state.areas[state.region].popularity += 2;
		state.stat['culture'] += 3;
		state.acts['culture'] += 1;
		return '현재 지역의 인지도가 올랐습니다. (' +  state.areas[state.region].popularity + 
		')\n문화 능력치가 올랐습니다. (' +  state.stat['culture'] + '/' + state.acts['culture'] + ')';
    }
};
