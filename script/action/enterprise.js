module.exports = {
	name: '기업 방문',
	point: 16,
	bg : 'building.jpg',
	scenario: function(state) {
		return [
		    '여러분 안녕하세요! 기호 1번 ' + state.character.name + ' 후보입니다.',
		    '기업활동 하기 좋은 환경을 만들겠습니다',
			'회사의 회의를 참관하였다.',
		];
    },
    applyEffect: function(state) {
		state.areas[state.region].popularity += 2;
		state.stat['economy'] += 2;
		state.acts['economy'] += 1;
		return '현재 지역의 인지도가 올랐습니다. (' +  state.areas[state.region].popularity + 
		')\n경제 능력치가 올랐습니다. (' +  state.stat['economy'] + '/' + state.acts['economy'] + ')';
    }
};
