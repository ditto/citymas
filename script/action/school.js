module.exports = {
	name: '학교 방문',
	point: 12,
	bg : 'school.jpg',
	scenario: function(state) {
		return [
		    '여러분 안녕하세요! 기호 1번 ' + state.character.name + ' 후보입니다.',
		    '북극패딩 입은새끼 나와',
		    '하라는 공부는 안하고!',
		];
    },
    applyEffect: function(state) {
		state.areas[state.region].popularity += 1;
		state.stat['education'] += 2;
		state.acts['education'] += 1;
		return '현재 지역의 인지도가 올랐습니다. (' +  state.areas[state.region].popularity + 
		')\n교육 능력치가 올랐습니다. (' +  state.stat['education'] + '/' + state.acts['education'] + ')';
    }
};
