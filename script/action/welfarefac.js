module.exports = {
	name: '복지시설 방문',
	point: 14,
	bg : 'welfarefac.jpg',
	scenario: function(state) {
		return [
		    '여러분 안녕하세요! 기호 1번 ' + state.character.name + ' 후보입니다.',
		    '선진국 수준의 복지를 제가 실현하겠습니다',
		];
    },
    applyEffect: function(state) {
		state.areas[state.region].popularity += 2;
		state.stat['welfare'] += 3;
		state.acts['welfare'] += 1;
		return '현재 지역의 인지도가 올랐습니다. (' +  state.areas[state.region].popularity + 
		')\n복지 능력치가 올랐습니다. (' +  state.stat['welfare'] + '/' + state.acts['welfare'] + ')';
    }
};
