module.exports = {
	name: '공업단지 방문',
	point: 15,
	bg : 'factory.jpg',
	scenario: function(state) {
		return [
		    '여러분 안녕하세요! 기호 1번 ' + state.character.name + ' 후보입니다.',
		    '새벽종이 울렸네~ 새 아침이 밝았네~',
			'근로자들의 처우 개선을 위해 노력하겠습니다.',
		];
    },
    applyEffect: function(state) {
		state.areas[state.region].popularity += 2;
		state.stat['welfare'] += 1;
		state.stat['economy'] += 1;
		state.acts['welfare'] += 1;
		return '복지,경제 능력치가 올랐습니다.';
    }
};
