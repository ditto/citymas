var cases = 0;
var up_or_down;

module.exports = {
	name: '거리 유세',
	point: 20,
	bg : 'street.jpg',
	scenario: function(state) {
	up_or_down = Math.floor(Math.random()*100);
	
	if(up_or_down <= 15){
	cases = -2;
		return [
		    '여러분 안녕하세요! 기호 1번 ' + state.character.name + ' 후보입니다.',
		    '님을 만나게 될 것입니다!',
			'개소리 집어쳐! 무슨 님을 만난다는 거야!',
			'괴한을 만났습니다. 유세현장이 아수라장이 됩니다.',
		];
	}
	else{
	cases = 0;
		return [
		    '여러분 안녕하세요! 기호 1번 ' + state.character.name + ' 후보입니다.',
		    '살림살이 좀 나아지셨습니까?',
		    state.character.name +'후보는 여러분을 위해 열심히 할것을 약속합니다!',
			state.character.name+'와쪄염 뿌잉뿌잉>_<',
		];
		}
	},
    applyEffect: function(state) {
		if (state.character.perk == 'fluent')
			state.areas[state.region].popularity += 5+cases;
		else
			state.areas[state.region].popularity += 4+cases;
    	return '현재 지역의 인지도가 올랐습니다. (' +  state.areas[state.region].popularity + ')';
    }
};
