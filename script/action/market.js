var up_or_down;
var cases;
module.exports = {
	name: '시장 방문',
	point: 18,
	bg : 'market.jpg',

	scenario: function(state) {
	up_or_down = Math.floor(Math.random()*100);
	if(state.month == 1&& up_or_down <= 40){
	cases = 3;
		return [
		    '설날을 앞두고 시장에 나왔습니다',
			'물가가 올라도 명절 대목엔 북적거리네요',
			'많은 시민들이 즐겁게 맞이해 줍니다.',
		];
						}	
	else if(up_or_down <= 20){
	cases = 2;
	return [
		    '장날이라 사람이 많네요.',
			'훈훈한 재래 시장을 다녀갑니다.',
		];
	}
	else
	{
	cases = 1;
	return [
		    '여러분 안녕하세요! 기호 1번 ' + state.character.name + ' 후보입니다.',
		    '살림살이 좀 나아 지셨습니까?',
			'시민을 위한 서울을 만들겠습니다.',
			'여기 국밥 한그릇 주세요!',
		];
	}
    },
    applyEffect: function(state) {
		state.areas[state.region].popularity += 3;
		state.stat['economy'] += cases;
		state.stat['welfare'] += cases;
		return '현재 지역의 인지도가 올랐습니다. (' +  state.areas[state.region].popularity + 
		')\n경제,복지 능력치가 올랐습니다.';
    }
};
