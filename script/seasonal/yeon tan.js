var gameData = require('../data');

module.exports = {
	name: '사랑의 연탄 배달',
		bg: 'yeontan.jpg',
	willHappen: function(state) {

	if (12 == state.month || 1 == state.month) 
	//	return (Math.floor(Math.random()*100) <= state.stat['welfare']);
		return true;
    },
	scenario: function(state) {
		
		
			return [
				'연탄 배달 자원봉사에 참여했습니다.',
				'이웃들이 따뜻한 겨울을 날수 있겠네요.',
				'참 잘했어요.',
			];
		
    },
    reactions: function(state) {
    	return [];
    },
    applyEffect: function(state, reaction) {
		
			state.stat['welfare'] += 2;
			return '복지 능력치가 2 올라갔습니다.';

    }
};
