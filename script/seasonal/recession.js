var gameData = require('../data');

module.exports = {
	name: '경기 침체',
	willHappen: function(state) {
		if (12==state.month)
			return true;
    },
	scenario: function(state) {
		return [
		    '수출률이 떨어지고 금리는 낮아져갑니다.',
			'경제 대책이 절실할 때입니다.',
		];
    },
    applyEffect: function(state, reaction) {

    	state.rule = gameData.supportrules.recession;
    	return '경제가 이슈로 떠올랐습니다.';
    }
};
