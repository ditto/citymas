var gameData = require('../data');
var up_or_down;

module.exports = {
	name: '기록적인 폭설',
		bg: 'snow.jpg',
	willHappen: function(state) {

	if (12 == state.month || 1 == state.month) 
		return Math.random() < 0.3;

    },
	scenario: function(state) {
		
		if (Math.floor(Math.random()*100) <= state.stat['environ'])
		{
			up_or_down = 0;
			return [
				'한반도 전체에 엄청난 눈이 왔습니다.',
				'대책을 제안해서 실효성을 거둡니다.',
			];
		}
		else 
		{
			up_or_down = 1;
			return [
					'한반도 전체에 엄청난 눈이 왔습니다.',
					'길도 막히고 집도 무너지는데 지금 밥이 넘어갑니까?',
				];
		}
    },
    reactions: function(state) {
    	return [];
    },
    applyEffect: function(state, reaction) {
		if(0==up_or_down)
		{
			state.stat['environ'] += 1;
			state.stat['welfare'] += 1;
			return '환경,복지 능력치가 1 올라갔습니다.';
		}
		
		else
		{
			state.stat['environ'] -= 1;
			state.stat['welfare'] -= 1;
			return '환경,복지 능력치가 1 내려갔습니다.';
		}
    }
};
