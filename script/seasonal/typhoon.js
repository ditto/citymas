var gameData = require('../data');
var up_or_down;

module.exports = {
	name: '태풍이 옵니다',
		bg:'typhoon.jpg',
	willHappen: function(state) {

	if (6 == state.month || 5 == state.month) 
		return Math.random() < 0.3;
	else
		return Math.random() < 0.05;

    },
	scenario: function(state) {
		
		if (Math.floor(Math.random()*100) <= state.stat['environ'])
		{
			up_or_down = 0;
			return [
				'태풍이한반도를 덮쳤습니다.',
				'재해방지대책을 내놓고 수해 복구에 참가합니다.',
			];
		}
		else 
		{
			up_or_down = 1;
			return [
					'태풍이 한반도를 덮쳤습니다.',
					'신경쓰지 않고 있다가 당황하는 모습을 보입니다.',
				];
		}
    },
    reactions: function(state) {
    	return [];
    },
    applyEffect: function(state, reaction) {
		if(0==up_or_down)
		{
			state.stat['environ'] += 1;
			state.stat['welfare'] += 1;
			return '환경,복지 능력치가 1 올라갔습니다.';
		}
		
		else
		{
			state.stat['environ'] -= 1;
			state.stat['welfare'] -= 1;
			return '환경,복지 능력치가 1 내려갔습니다.';
		}
    }
};
