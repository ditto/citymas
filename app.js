
/**
 * Module dependencies.
 */

var express = require('express'),
    fs = require('fs');

var app = module.exports = express.createServer();

// Configuration

app.configure(function(){
  app.set('views', __dirname + '/views');
  app.set('view engine', 'ejs');
  app.use(express.cookieParser());
  app.use(express.session({ secret: 'citymas' }))
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(__dirname + '/public'));
});

app.configure('development', function(){
  app.use(express.errorHandler({ dumpExceptions: true, showStack: true })); 
});

app.configure('production', function(){
  app.use(express.errorHandler()); 
});

app.dynamicHelpers({
	error: function (req, res) { return req.flash('error'); },
    info: function (req, res) { return req.flash('info'); },
});

// Game Data
var gameData = require('./script/data');
var consts = gameData.consts;

var scripts = {};
['action', 'event', 'seasonal'].forEach(function(dir) {
	scripts[dir] = {};
	fs.readdirSync('./script/' + dir).forEach(function(file) {
		if (file.substring(file.length - 3) == '.js') {
			var id = file.substring(0, file.length - 3);
			scripts[dir][id] = require('./script/' + dir + '/' + file);
			scripts[dir][id].id = id;
        }
    });
});

// State
var state = {
    character: null,
    month: 11,
	week: 0,
    point: 100,
    stat: null,
	acts: null,
	areas: null,
    region: 0,
	rule: gameData.supportrules.normal,
    surveyCount: 0,
    lastSurveyWeek: -1,
};

function calculateMonth()
{
	var monthbefore = state.month;
	state.month = 1 + Math.floor(consts.START_MONTH - 1 + state.week / 4) % 12;
	
	return (monthbefore!=state.month);		// 달이 바뀌었는지 여부 리턴 
}

function monthStart()
{
	// 행동 횟수 초기화
	for (var i = 0; i < gameData.stats.length; i++) {
		var id = gameData.stats[i].id;
		state.acts[id] = 0;
	}
	
	// 계절 이벤트 발생
	var t_seasonal = [];

	for (var id in scripts.seasonal)
		if (scripts.seasonal[id].willHappen(state))
			t_seasonal.push(scripts.seasonal[id]);
	
	if(t_seasonal.length>0)
		return t_seasonal[Math.floor(Math.random()*t_seasonal.length)].id;
	
	return null;
}

function updateSupport()
{
	//rule에 따라 지지도 계산
	for (var i=0; i<state.areas.length; i++)
	{
		var tempsupport = 0;
		var tempval;
		
		for(var id in state.rule)
		{
			tempval = state.stat[id]*state.acts[id]*state.rule[id];
			
			if(tempval>0)
				tempval = Math.log(tempval);
			else if (tempval<0)
				tempval = -Math.log(tempval);
			
			tempsupport += tempval/Math.LN10;
		}
		
		state.areas[i].support += state.areas[i].popularity*tempsupport/50;
		
		if (state.areas[i].support >= 100)
			state.areas[i].support = 100;
	}
}

state.areas = [];
for (var i = 0; i < gameData.areas.length; i++)
	state.areas.push({popularity: 10, support: 0});

function triggerEvents()
{
	if(2 == state.week) return scripts.event['examination'].id;
	
	var t_events = [];

	for (var id in scripts.event)
		if (scripts.event[id].willHappen(state))
			t_events.push(scripts.event[id]);
	
	if(t_events.length>0)
		return t_events[Math.floor(Math.random()*t_events.length)].id;
		
	return null;
}

// Routes

app.helpers({
	gameData: gameData,
	consts: consts,
});

app.dynamicHelpers({
	state: function() { return state },
});

app.get('/', function(req, res) {
    if (state.character == null) {
        res.render('create_character', {stats: gameData.stats, perks: gameData.perks});
    } else {
    	var availActions = [];
        // 일주일에 한번만 할 수 있음.
        if (state.week != state.lastSurveyWeek &&
            state.week != consts.NWEEKS - 1 &&
            state.surveyCount < consts.MAX_SURVEYS)
            availActions.push(scripts.action.survey);

        gameData.areas[state.region].actions.forEach(function(id) {
            var action = scripts.action[id];
			if (action)
				availActions.push(action);
        });
    	res.render('main', {areas: gameData.areas, actions: availActions});
    }
});

app.get('/do/:action', function(req, res) {
	var id = req.param('action');
	var action = scripts.action[id];
	if (state.point < action.point) {
		req.flash('error', '행동력이 부족합니다.');
		res.redirect('/');
		return;
    }

	state.point -= action.point;

	res.render('action_scene', {
		action: action,
		scenario: action.scenario(state),
		effect: action.applyEffect(state),
	});
});

app.get('/event', function(req, res) {
	var event = scripts.event[state.currentEvent];

	res.render('event_scene', {
		event: event,
		scenario: event.scenario(state),
		reactions: event.reactions(state),
	});
});

app.post('/event', function(req, res) {
	var event = scripts.event[state.currentEvent];
	if (null !== req.param('reaction'))
		var effect = event.applyEffect(state, parseInt(req.param('reaction')));
	req.flash('info', effect);
	res.redirect('/');
});

app.get('/seasonal', function(req, res) {
	var event = scripts.seasonal[state.currentEvent];

	res.render('seasonal_scene', {
		event: event,
		scenario: event.scenario(state),
		effect: event.applyEffect(state),
	});
});

// 이동
app.post('/go', function(req, res) {
	if (state.point < consts.MOVE_POINT) {
		req.flash('error', '행동력이 부족합니다.');
		res.redirect('/');
		return;
    }
	
	state.point -= consts.MOVE_POINT;
	state.region = parseInt(req.param('target'));
	res.redirect('/');
});

// 휴식
app.post('/rest', function(req, res) {
	
	updateSupport();
	
	state.point = consts.INITIAL_POINT;
	if (state.character.perk == 'diligent')
		state.point+=15;
	
	for (var i=0; i<state.areas.length; i++)
		state.areas[i].popularity = Math.floor(state.areas[i].popularity*0.9);
		
	if(consts.NWEEKS == (++state.week))
	{
		res.redirect('/ending');
		return;
	}
	
	if (calculateMonth()) {
		var seasonalEvent = monthStart();
		if (seasonalEvent != null) {
			state.currentEvent = seasonalEvent;
			res.redirect('/seasonal');
			return;
        }
    }
	//이벤트 발생
	
	var triggeredEvent = triggerEvents();
	if (triggeredEvent == null)
    	res.redirect('/');
    else {
    	state.currentEvent = triggeredEvent;
    	res.redirect('/event');
    }
});

// 캐릭터 생성
app.post('/start', function(req, res) {
	if (state.character == null) {
		state.character = {
			name: req.param('name'),
			party: req.param('party') + '당',
			photo: req.param('face'),
			perk : req.param('perk'),
        };

        state.stat = {};
		state.acts = {};
        for (var i = 0; i < gameData.stats.length; i++) {
        	var id = gameData.stats[i].id;
        	state.stat[id] = parseInt(req.param(id));
			state.acts[id] = 0;
        }
		
		//특성 관련 설정
		if (state.character.perk == 'diligent')
			state.point+=15;
		else if (state.character.perk == 'envactiv')
			state.stat['environ']+=10;
		else if (state.character.perk == 'oriartis')
			state.stat['culture']+=10;
		else if (state.character.perk == 'socactiv')
			state.stat['welfare']+=10;
		else if (state.character.perk == 'oribusin')
			state.stat['economy']+=10;
			
		res.redirect('/');
    }
});

// 엔딩
app.get('/ending', function(req, res) {
//    if (state.week < consts.NWEEKS)
//        throw new Error('Not end yet...');

    var support = 0;
    state.areas.forEach(function(s) { support += s.support; });
    support /= state.areas.length;

    res.render('ending', {result: support >= 50, support: support});
});

// Backup & Restore
var BACKUP_PATH = './backup.json';

function saveState() {
	fs.writeFile(BACKUP_PATH, JSON.stringify(state));
	console.log('Saved state');
}

function restoreState() {
	try {
    	state = JSON.parse(fs.readFileSync(BACKUP_PATH, 'utf-8'));
    	console.log('Restored from file');
    } catch (e) {
    	console.log('No saved state');
    }
}

if (process.argv.indexOf('--reset') === -1)
    restoreState();

// save state every 30 secs
setInterval(saveState, 30000);

// Server Main Loop
app.listen(3000);
console.log("Express server listening on port %d in %s mode", app.address().port, app.settings.env);
